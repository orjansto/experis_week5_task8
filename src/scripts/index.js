import '../styles/index.scss';


function renderPhotos(photos) {
    var l = photos.length;
    var innHTML = "";
    $.each(photos, (i, photo)=>{      
        if(i==0){
            console.log("0!");
            innHTML += `<div class="col-6 md-4">`;
        }
        if(i==(Math.ceil(l/2))){
            innHTML += `
                </div>
                <div class="col-6 md-4">`;
        }
        innHTML += createCard(photo);
        if(i==(l-1)){
            innHTML += `</div>`;
            document.getElementById('row').innerHTML += innHTML;
        }
    });
}

function renderPhotosNew(photos) {
    var l = photos.length;
    var innHTML = "";
    var innHTMLcol1 = `<div class="col-6 md-4">`;
    var col1Height = 0;
    var col2Height = 0;
    var innHTMLcol2 = `<div class="col-6 md-4">`;
    $.each(photos, (i, photo)=>{
        if(col1Height <= col2Height){
            innHTMLcol1 += createCard(photo);
            col1Height += (photo.height/photo.width);
        }else{
            innHTMLcol2 += createCard(photo);
            col2Height += ((photo.height)/(photo.width));
        }
        if(i==(l-1)){
            innHTMLcol1 += `</div>`;
            innHTMLcol2 += `</div>`;
            innHTML += innHTMLcol1;
            innHTML += innHTMLcol2;
            document.getElementById('row').innerHTML += innHTML;
        }
    });
}



function createCard(photo) {
    return `
    <div class="card" style="18rem;">
        <div class="card-img">
            <img class="card-img-top" src="${photo.src.large2x}"/>
            <div class="card-body">
                <a id="text" href="${photo.photographer_url}">${photo.photographer}</a>
            </div>
        </div>      
    </div>
    `;
}
function createSpinner(){
    return `
    <div class="row mt-5 mb.5">
        <div class="col-6"></div>
        <div class="spinner-border"></div>
    </div>
    `;
}


var numberOfPics = 12;
var searchQuery = 'Swimsuit';


const apiKey = '563492ad6f917000010000016055e57c01e044b989be47b73d175bbc';
var apiUrl = `https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=${searchQuery}&per_page=${numberOfPics}&page=1`;
$(document).ready(()=>{   
    $.ajax({
        
        url: `${apiUrl}`,
        type: 'GET',
        'headers': {
            'Authorization': `Bearer ${apiKey}`
           }
        }).done(data => {
            document.getElementById('imgNotThere').innerHTML = "";
            renderPhotosNew(data.photos); 
        });
});
window.helloWorld = function() {
    console.log('HEllooOooOOo!');
};


$('#search-button').click(function(event){
    searchPhotos();
});

$('.photoInput').keydown(function (e){
    if(e.keyCode == 13){
        searchPhotos();
    }
});


function searchPhotos(){
    searchQuery = $('input').val();
    console.log($('input').val());
    if($('#search-number').val()){
        numberOfPics = $('#search-number').val();
    }
    apiUrl = `https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=${searchQuery}&per_page=${numberOfPics}&page=1`;
    document.getElementById('row').innerHTML = "";
    document.getElementById('imgNotThere').innerHTML = createSpinner();
    $.ajax({
        
        url: `${apiUrl}`,
        type: 'GET',
        'headers': {
            'Authorization': `Bearer ${apiKey}`
           }
        }).done(data => {
            document.getElementById('imgNotThere').innerHTML = "";
            renderPhotosNew(data.photos);        
        });
}

