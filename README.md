﻿**Experis Academy, Norway**

**Authors:**
* **Ørjan Storås**

# Experis Week5 Task 8
## Task 8
- [x] The website should serve the images dynamically from the Pexels API using jQuery.
- [x] The website should look professional and make use of Twitter Bootstrap 4.
- [x] The website should be deployed using Heroku or a similar hosting solution where it is publicly addressable online.